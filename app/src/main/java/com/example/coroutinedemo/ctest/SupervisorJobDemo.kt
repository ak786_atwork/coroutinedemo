package com.example.coroutinedemo.ctest

import kotlinx.coroutines.*
import java.lang.Exception

/*
* Warning: A SupervisorJob only works as described when it’s part of a scope: either created using
* supervisorScope or CoroutineScope(SupervisorJob()).
* */

fun main(args: Array<String>) {

    startJobFail()
}


fun startJobFail() = runBlocking {

    // Scope handling coroutines for a particular layer of my app
    val scope = CoroutineScope(Job())
    scope.launch {
        // Child 1
        println("child 1 started working on ${Thread.currentThread().name} ")
        delay(2000)
        println("child 1 going to crash : exception ")
        throw Exception("failed co routine")
        println("child 1 completed")
    }
    scope.launch {
        // Child 2
        println("child 2 started working on ${Thread.currentThread().name}")
        delay(3000)
        println("child 2 completed")
    }

//    delay(10000)
}