package com.example.coroutinedemo.ctest

import kotlinx.coroutines.*

fun main(args: Array<String>) = runBlocking<Unit> {

    val job = launch(Dispatchers.Default) {
        println("job started")
        delay(1000)
        println("job ended")
    }

//    job.cancel()      //it will just cancel, no effect
    job.join()

    print("now main quits")

}


/**
 * Job.join suspends a coroutine until the work is completed. Together with job.cancel it behaves as you’d expect:
If you’re calling job.cancel then job.join, the coroutine will suspend until the job is completed.
Calling job.cancel after job.join has no effect, as the job is already completed.

 */