package com.example.coroutinedemo.ctest

import kotlinx.coroutines.*

fun main(args: Array<String>) = runBlocking<Unit> {
    val startTime = System.currentTimeMillis()
    val job = launch (Dispatchers.Default) {
        var nextPrintTime = startTime
        var i = 0
        while (i < 5) {
            ensureActive()          //enables co operative cancellation
            // print a message twice a second
            if (System.currentTimeMillis() >= nextPrintTime) {
                println("Hello ${i++}")
                nextPrintTime += 500L
            }
        }
    }
    delay(1000L)
    println("Cancel!")
    job.cancel()
    println("Done!")
}

/**
 * Making your coroutine work cancellable You need to make sure that all the coroutine work you’re implementing
 * is cooperative with cancellation, therefore you need to check for cancellation periodically or before beginning
 * any long running work. For example, if you’re reading multiple files from disk, before you start reading each file,
 * check whether the coroutine was cancelled or not.
 *
 * All suspend functions from kotlinx.coroutines are cancellable: withContext, delay etc. So if you’re using any of them
 * you don’t need to check for cancellation and stop execution or throw a CancellationException. But, if you’re not using them,
 * to make your coroutine code cooperative we have two options:
   1. Checking job.isActive or ensureActive()
    2. Let other work happen using yield()
 *
 *
 *
 * Let other work happen using yield()
If the work you’re doing is 1) CPU heavy, 2) may exhaust the thread pool and 3) you want to allow the thread to do other work
without having to add more threads to the pool, then use yield(). The first operation done by yield will be checking for completion
and exit the coroutine by throwing CancellationException if the job is already completed.
yield can be the first function called in the periodic check, like ensureActive() mentioned above.
 *
 * */