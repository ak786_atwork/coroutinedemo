package com.example.coroutinedemo.ctest

import kotlinx.coroutines.*

fun main(args: Array<String>) {
    main5()
}

fun main1() = runBlocking<Unit> {
    val startTime = System.currentTimeMillis()
    val job = launch(Dispatchers.Default) {
        var nextPrintTime = startTime
        var i = 0
        while (i < 5 && isActive) {
            // print a message twice a second
            if (System.currentTimeMillis() >= nextPrintTime) {
                println("Hello ${i++}")
                nextPrintTime += 500L
            }
        }

        println("perform clean up here")
    }
    delay(1000L)
    println("Cancel!")
    job.cancel()
    println("Done!")

}

fun main2() = runBlocking {
    val job = launch {
        try {
            dowork()
        } catch (e: CancellationException) {
            println("Work cancelled!")
        } finally {
            println("Clean up!")
        }
    }
    delay(2000L)
    println("Cancel!")
    job.cancel()
    println("Done!")
}


/*But, if the cleanup work we need to execute is suspending, the code above won’t work anymore,
 as once the coroutine is in Cancelling state, it can’t suspend anymore.*/
fun main3() = runBlocking {
    val job = launch {
        try {
            dowork()
        } catch (e: CancellationException) {
            println("Work cancelled!")
        } finally {
            cleanUp()   //suspending function
        }
    }
    delay(1000L)
    println("Cancel!")
    job.cancel()
    println("Done!")
}

suspend fun cleanUp() {
    println("clean up started")
    delay(2000)
    println("clean up finished")
}

suspend fun dowork() {
    delay(6000)
}


/*To be able to call suspend functions when a coroutine is cancelled, we will need to switch the cleanup work we need to do in a
 NonCancellable CoroutineContext. This will allow the code to suspend and will keep the coroutine in the Cancelling state until
 the work is done.*/

fun main4() = runBlocking {
    val job = launch {
        try {
            dowork()
        } catch (e: CancellationException) {
            println("Work cancelled!")
        } finally {
            withContext(NonCancellable) {
                cleanUp()   //suspending function
            }
        }
    }
    delay(1000L)
    println("Cancel!")
    job.cancel()
    println("Done!")
}

fun main5() = runBlocking {
    val job = launch {
        work()
    }
    delay(1000L)
    println("Cancel!")
    job.cancel()
    println("Done!")
}

suspend fun work() {
    return suspendCancellableCoroutine { continuation ->
        continuation.invokeOnCancellation {
            // do cleanup
            println("clean up started----")
            println("clean up finished---")
        }
        // rest of the implementation

        println("work started ")
        println("work finished ")
    }

}