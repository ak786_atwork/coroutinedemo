package com.example.coroutinedemo.ctest

import kotlinx.coroutines.*

fun main(args: Array<String>) = runBlocking<Unit> {

    val deferred = async(Dispatchers.Default) {
        println("job started")
        delay(1000)
        println("job ended")
    }

    deferred.cancel()

    /*Exception in thread "main" kotlinx.coroutines.JobCancellationException: DeferredCoroutine was cancelled; job=DeferredCoroutine{Cancelled}@726f3b58*/
    val result = deferred.await()

}




/*

the role of await is to suspend the coroutine until the result is computed; since the coroutine is cancelled,
the result cannot be computed. Therefore, calling await after cancel leads to JobCancellationException: Job was cancelled.
On the other hand, if you’re calling deferred.cancel after deferred.await nothing happens, as the coroutine is already completed.

*/
