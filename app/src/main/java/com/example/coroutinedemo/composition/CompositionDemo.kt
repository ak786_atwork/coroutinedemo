package com.example.coroutinedemo.composition


import kotlinx.coroutines.*
import java.lang.Exception
import kotlin.system.*


fun main(array: Array<String>) {

//    demo1()
//    demo2()
//    demo3()
//    demo4()
//    demo5()
    demo6()


}


/**
 *  In practice we do this if we use the result of the rst function to make a decision on whether we need to invoke the second one or to decide on how to invoke it.
 *  We use a normal sequential invocation, because the code in the coroutine, just like in the regular code, is sequential by default.
 * */
fun demo1() = runBlocking<Unit> {

    val time = measureTimeMillis {
        val one = doSomethingUsefulOne()
        var two = 0
        if (one != 13)
            two = doSomethingUsefulTwo()

        println("The answer is ${one + two}")
    }

    println("Completed in $time ms")

}


suspend fun doSomethingUsefulOne(): Int {
    println("doSomethingUsefulOne is executing")
    delay(1000L) // pretend we are doing something useful here
    return 13
}

suspend fun doSomethingUsefulTwo(): Int {
    println("doSomethingUsefulTwo is executing")
    delay(1000L) // pretend we are doing something useful here, too
    return 29
}


/**************************************************************************************************/

/**
 * Conceptually, async is just like launch. It starts a separate coroutine which is a light-weight thread that works concurrently
 * with all the other coroutines. The dierence is that launch returns a Job and does not carry any resulting value, while async returns
 * a Deferred — a light-weight non-blocking future that represents a promise to provide a result later.
 *
 * You can use .await() on a deferred value to get its eventual result, but Deferred is also a Job, so you can cancel it if needed.

 * */

/**
 * What if there are no dependencies between invocations of doSomethingUsefulOne and doSomethingUsefulTwo
 * and we want to get the answer faster, by doing both concurrently
 * */

fun demo2() = runBlocking<Unit> {

    val time = measureTimeMillis {
        val one = async { doSomethingUsefulOne() }
        val two = async { doSomethingUsefulTwo() }

//        println("The answer is ${one.await() + two.await()}")
    }

    println("Completed in $time ms")
}


/**************************************************************************/

/**
 * Optionally, async can be made lazy by setting its start parameter to CoroutineStart.LAZY.
 * In this mode it only starts the coroutine when its result is required by await, or if its Job's start function is invoked.
 * */

/**
 * Note that if we just call await in println without rst calling start on individual coroutines,
 * this will lead to sequential behavior, since await starts the coroutine execution and waits for its finish,
 * which is not the intended use-case for laziness.
 *
 * The use-case for async(start = CoroutineStart.LAZY) is a replacement for the standard lazy function
 * in cases when computation of the value involves suspending functions.

 * */

fun demo3() = runBlocking<Unit> {

    val time = measureTimeMillis {
        val one = async(start = CoroutineStart.LAZY) { doSomethingUsefulOne() }
        val two = async(start = CoroutineStart.LAZY) { doSomethingUsefulTwo() }

        // some computation
        one.start() // start the first one
        two.start() // start the second one

        println("The answer is ${one.await() + two.await()}")
    }

    println("Completed in $time ms")
}






/******************************************************************************************************/


// note that we don't have `runBlocking` to the right of `main` in this example


/**
 * We can dene async-style functions that invoke doSomethingUsefulOne and doSomethingUsefulTwo asynchronously using the async coroutine builder
 * with an explicit GlobalScope reference. We name such functions with the "…Async" sux to highlight the fact that
 * they only start asynchronous computation and one needs to use the resulting deferred value to get the result* */

fun demo4() {

    val time = measureTimeMillis {
        // we can initiate async actions outside of a coroutine
        val one = somethingUsefulOneAsync()
        val two = somethingUsefulTwoAsync()

        // but waiting for a result must involve either suspending or blocking.
        // here we use `runBlocking { ... }` to block the main thread while waiting for the result

        runBlocking {
            println("The answer is ${one.await() + two.await()}")
        }
    }

    println("Completed in $time ms")
}



fun somethingUsefulOneAsync() = GlobalScope.async {
    doSomethingUsefulOne()
}

fun somethingUsefulTwoAsync() = GlobalScope.async {
    doSomethingUsefulTwo()
}






/***********************************************************************************************/


/**
 * Consider what happens if between the val one = somethingUsefulOneAsync() line and one.await() expression there is some
 * logic error in the code and the program throws an exception and the operation that was being performed by the program aborts.
 * Normally, a global error-handler could catch this exception, log and report the error for developers,
 * but the program could otherwise continue doing other operations. But here we have somethingUsefulOneAsync still running in the background,
 * even though the operation that initiated it was aborted. This problem does not happen with structured concurrency
 * */

/**
 * This way, if something goes wrong inside the code of the concurrentSum function and it throws an exception,
 * all the coroutines that were launched in its scope will be cancelled.

Cancellation is always propagated through coroutines hierarchy
 * */

fun demo5() = runBlocking<Unit> {

    val time = measureTimeMillis {
        println("The answer is ${concurrentSum()}")
    }

    println("Completed in $time ms")
}

suspend fun concurrentSum(): Int = coroutineScope {

    val one = async { doSomethingUsefulOne() }
    val two = async { doSomethingUsefulTwo() }

    one.await() + two.await()
}





/*****************************************************************************************************************/


fun demo6() = runBlocking<Unit> {

    try {
        failedConcurrentSum()
    } catch(e: ArithmeticException) {
        println("Computation failed with ArithmeticException")
    }

}



suspend fun failedConcurrentSum(): Int = coroutineScope {

    val one = async<Int> {
        try {
            delay(Long.MAX_VALUE) // Emulates very long computation
            42

        } finally {
            println("First child was cancelled")
        }
    }

    val two = async<Int> {
        println("Second child throws an exception")
        throw ArithmeticException()
    }

    one.await() + two.await()
}


