package com.example.coroutinedemo.basic

import kotlinx.coroutines.*


/*all producing same effect*/

fun main(args : Array<String>) {

//  demo7()
  demo8()

}


/*Here we are launching a new coroutine in the GlobalScope, meaning that the lifetime of the new coroutine
 is limited only by the lifetime of the whole application. */

fun demo1() {
    GlobalScope.launch {
        delay(1000)
        println("world")
    }

    println("hello ")

    Thread.sleep(2000)
}

fun demo2() {
    GlobalScope.launch {
        delay(1000)
        println("world")
    }

    println("hello ")

    runBlocking { delay(2000) } //runs on main thread to keep process (jvm) alive
}


fun demo3() = runBlocking {
    GlobalScope.launch {
        delay(1000)
        println("world")
    }

    println("hello ")

    delay(2000)     //runs on main thread
}


fun demo4() = runBlocking {
    val job = GlobalScope.launch {
        delay(2000)
        println("world")
    }

    print("hello ")

    job.join()      //waiting in non- blocking fashion
}


/*Every coroutine builder, including runBlocking, adds an instance of CoroutineScope to the scope of its code block.
 We can launch coroutines in this scope without having to join them explicitly, because an outer coroutine
(runBlocking in our example) does not complete until all the coroutines launched in its scope complete. */

fun demo5() = runBlocking {

    launch {
        delay(2000)
        println("world")
    }

    print("hello ")
}

/*
 it is possible to declare your own scope using coroutineScope builder. It creates a coroutine scope and does not complete until
 all launched children complete. The main dierence between runBlocking and coroutineScope is that the latter does not block the
 current thread while waiting for all children to complete.
* */
fun demo6() =  runBlocking { // this: CoroutineScope

    launch {
        delay(200L)
        println("Task from runBlocking")
    }

    coroutineScope { // Creates a coroutine scope

        launch {
            delay(2000L)
            println("Task from nested launch")
        }

        delay(100L)
        println("Task from coroutine scope") // This line will be printed before the nested launch
    }

    println("Coroutine scope is over") // This line is not printed until the nested launch completes
}


//Global coroutines are like daemon threads
/*The following code launches a long-running coroutine in GlobalScope that prints "I'm sleeping" twice a second and
 then returns from the main function after some delay*/

fun demo7() = runBlocking {
    GlobalScope.launch {
        println("thread name = ${Thread.currentThread().name}  damemon = ${Thread.currentThread().isDaemon}")

        repeat(1000) { i ->
            println("I'm sleeping $i ...")
            delay(500L)
        }
    }

    delay(1300L) // just quit after delay

}

fun demo8() = runBlocking {

    coroutineScope {
        println("thread name = ${Thread.currentThread().name}  damemon = ${Thread.currentThread().isDaemon}")

        launch {
            repeat(1000) { i ->
                println("I'm sleeping $i ...")
                delay(500L)
            }
        }
    }

    delay(1300L) // just quit after delay

}


