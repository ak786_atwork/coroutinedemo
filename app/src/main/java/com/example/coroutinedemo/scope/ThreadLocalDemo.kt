package com.example.coroutinedemo.scope


import kotlinx.coroutines.*

/**
 * Thread-local data

Sometimes it is convenient to have an ability to pass some thread-local data to or between coroutines.
However, since they are not bound to any particular thread, this will likely lead to boilerplate if done manually.

For ThreadLocal, the asContextElement extension function is here for the rescue.
It creates an additional context element which keeps the value of the given ThreadLocal and restores it every time the coroutine switches its context.
 *
 * */


val threadLocal = ThreadLocal<String?>() // declare thread-local variable

fun main() = runBlocking<Unit> {

    threadLocal.set("main")
    println("Pre-main, current thread: ${Thread.currentThread()}, thread local value: '${threadLocal.get()}'")

    val job = launch(Dispatchers.Default + threadLocal.asContextElement(value = "launch")) {
        println("Launch start, current thread: ${Thread.currentThread()}, thread local value: '${threadLocal.get()}'")
        yield()
        println("After yield, current thread: ${Thread.currentThread()}, thread local value: '${threadLocal.get()}'")
    }

    job.join()
    println("Post-main, current thread: ${Thread.currentThread()}, thread local value: '${threadLocal.get()}'")
}



/**
 * It's easy to forget to set the corresponding context element.
 * The thread-local variable accessed from the coroutine may then have an unexpected value, if the thread running the coroutine is dierent.
 * To avoid such situations, it is recommended to use the ensurePresent method and failfast on improper usages.
 *
 * ThreadLocal has rst-class support and can be used with any primitive kotlinx.coroutines provides.
 * It has one key limitation, though: when a thread-local is mutated, a new value is not propagated to the coroutine caller
 * (because a context element cannot track all ThreadLocal object accesses), and the updated value is lost on the next suspension.
 *
 * Use withContext to update the value of the thread-local in a coroutine, see asContextElement for more details.
 * Alternatively, a value can be stored in a mutable box like class Counter(var i: Int), which is, in turn, stored in a thread-local variable.
 * However, in this case you are fully responsible to synchronize potentially concurrent modications to the variable in this mutable box.
 *
 * For advanced usage, for example for integration with logging MDC, transactional contexts or any other libraries which internally use
 * thread-locals for passing data, see documentation of the ThreadContextElement interface that should be implemented.

 *
 * */