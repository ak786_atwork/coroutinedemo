package com.example.coroutinedemo.scope

import kotlinx.coroutines.*



/**
 * We manage the lifecycles of our coroutines by creating an instance of CoroutineScope tied to the lifecycle of our activity.
 * A CoroutineScope instance can be created by the CoroutineScope() or MainScope() factory functions.
 *
 * The former creates a general-purpose scope, while the latter creates a scope for UI applications and uses Dispatchers.Main as the default dispatcher:

 * */
class Activity : CoroutineScope by CoroutineScope(Dispatchers.Default) {

    fun destroy() {
        cancel() // Extension on CoroutineScope
    }

    // to be continued ...



    // class Activity continues

    fun doSomething() {
        // launch ten coroutines for a demo, each working for a different time
        repeat(10) { i ->

            launch {
                delay((i + 1) * 200L) // variable delay 200ms, 400ms, ... etc
                println("Coroutine $i is done")
            }
        }
    }

} // class Activity ends



fun main() = runBlocking<Unit> {

    val activity = Activity()
    activity.doSomething() // run test function
    println("Launched coroutines")

    delay(500L) // delay for half a second

    println("Destroying activity!")

    activity.destroy() // cancels all coroutines

    delay(1000) // visually confirm that they don't work

}