package com.example.coroutinedemo.timeout

import kotlinx.coroutines.*

fun main(array: Array<String>) {
//    demo1()
    demo2()
}


/**
 * The most obvious practical reason to cancel execution of a coroutine is because its execution time has exceeded some timeout.
 * While you can manually track the reference to the corresponding Job and launch a separate coroutine to cancel the tracked one after delay,
 *
 * there is a ready to use withTimeout function that does it
 * */
fun demo1()  = runBlocking {


    //throws Exception in thread "main" kotlinx.coroutines.TimeoutCancellationException: Timed out waiting for 1300 ms
    // handling in demo2
    withTimeout(1300L) {
        repeat(1000) { i ->
            println("I'm sleeping $i ...")
            delay(500L)
        }
    }

}




/**
 * The TimeoutCancellationException that is thrown by withTimeout is a subclass of CancellationException. We have not seen its stack trace printed on the console before.
 * That is because inside a cancelled coroutine CancellationException
 * is considered to be a normal reason for coroutine completion. However, in this example we have used withTimeout right inside the main function
 * */

fun demo2()  = runBlocking {

    val result = withTimeoutOrNull(1300L) {

        repeat(1000) { i ->
            println("I'm sleeping $i ...")
            delay(500L)

        }

        "Done" // will get cancelled before it produces this result
    }

    println("Result is $result")

}