package com.example.coroutinedemo.cancel

import kotlinx.coroutines.*
import java.lang.Exception

fun main(args: Array<String>) {
//    demo1()
//    demo2()
    demo3()
}


fun demo1() = runBlocking {

    val job = launch {
        repeat(1000) { i ->

            println("job: I'm sleeping $i ...")
            delay(500L)
        }
    }
    delay(1300L) // delay a bit

    println("main: I'm tired of waiting!")

    //same affect as cancel and then join
    job.cancelAndJoin()

//    job.cancel() // cancels the job
//    job.join() // waits for job's completion

    println("main: Now I can quit.")

}


//Cancellable suspending functions throw CancellationException on cancellation which can be handled in the usual way.
// For example, try {...} finally {...} expression and Kotlin use function execute their nalization actions normally
// when a coroutine is cancelled
//Both join and cancelAndJoin wait for all nalization actions to complete,
fun demo2() = runBlocking {

    val job = launch {
        try {
            repeat(1000) { i ->
                println("job: I'm sleeping $i ...")
                delay(500L)
            }
        } catch (exception: Exception) {
            println(exception.toString())   //kotlinx.coroutines.JobCancellationException: StandaloneCoroutine was cancelled; job=StandaloneCoroutine{Cancelling}@48533e64
        } finally {
            println("cleanup job: I'm running finally")
        }
    }
    delay(1300L) // delay a bit
    println("main: I'm tired of waiting!")
    job.cancelAndJoin() // cancels the job and waits for its completion
    println("main: Now I can quit.")

}


/**
* Any attempt to use a suspending function in the finally block of the previous example causes CancellationException,
 * because the coroutine running this code is cancelled. Usually, this is not a problem, since all well-behaving closing operations
 * (closing a le, cancelling a job, or closing any kind of a communication channel) are usually non-blocking and do not involve any
 * suspending functions. However, in the rare case when you need to suspend in a cancelled coroutine you can wrap the corresponding code in
 * withContext(NonCancellable) {...} using withContext function and NonCancellable context
 * */
fun demo3() = runBlocking {

    val job = launch {

        try {
            repeat(1000) { i ->
                println("job: I'm sleeping $i ...")
                delay(500L)
            }
        } finally {
            withContext(NonCancellable) {
                println("job: I'm running finally")
                delay(1000L)

                println("job: And I've just delayed for 1 sec because I'm non-cancellable")
            }
        }
    }
    delay(1300L) // delay a bit
    println("main: I'm tired of waiting!")
    job.cancelAndJoin() // cancels the job and waits for its completion

    println("main: Now I can quit.")
}