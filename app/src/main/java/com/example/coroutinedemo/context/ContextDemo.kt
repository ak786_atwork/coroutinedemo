package com.example.coroutinedemo.context

import kotlinx.coroutines.*

fun main() {

//    demo1()
//    demo2()
//    demo3()
//    demo4()
//    demo5()
//    demo6()
//    demo7()
//    demo8()
    demo9()

}


/**
 * The coroutine context includes a coroutine dispatcher (see CoroutineDispatcher) that determines what thread
 * or threads the corresponding coroutine uses for its execution.
 *
 * The coroutine dispatcher can conne coroutine execution to a specic thread, dispatch it to a thread pool, or let it run unconned
 * */

fun demo1() = runBlocking<Unit> {

    launch { // context of the parent, main runBlocking coroutine
        println("main runBlocking      : I'm working in thread ${Thread.currentThread().name}")
    }

    launch(Dispatchers.Unconfined) { // not confined -- will work with main thread
        println("Unconfined            : I'm working in thread ${Thread.currentThread().name}")
    }

    launch(Dispatchers.Default) { // will get dispatched to DefaultDispatcher
        println("Default               : I'm working in thread ${Thread.currentThread().name}")
    }

    launch(newSingleThreadContext("MyOwnThread")) { // will get its own new thread
        println("newSingleThreadContext: I'm working in thread ${Thread.currentThread().name}")
    }

}


/*********************************************************************************************************************/

/**
 * The coroutine context includes a coroutine dispatcher (see CoroutineDispatcher) that determines what thread or threads the
 * corresponding coroutine uses for its execution. The coroutine dispatcher can conne coroutine execution to a specic thread,
 * dispatch it to a thread pool, or let it run unconned.
 *
 * All coroutine builders like launch and async accept an optional CoroutineContext parameter that can be used to explicitly
 * specify the dispatcher for the new coroutine and other context elements.
 *
 * */

/**
 * When launch { ... } is used without parameters, it inherits the context (and thus dispatcher) from the CoroutineScope
 * it is being launched from. In this case, it inherits the context of the main runBlocking coroutine which runs in the main thread.
 *
Dispatchers.Unconfined is a special dispatcher that also appears to run in the main thread, but it is, in fact, a dierent mechanism
that is explained later.

The default dispatcher that is used when coroutines are launched in GlobalScope is represented by Dispatchers.Default and uses a
shared background pool of threads, so launch(Dispatchers.Default) { ... } uses the same dispatcher as GlobalScope.launch { ... }.

newSingleThreadContext creates a thread for the coroutine to run. A dedicated thread is a very expensive resource. In a real application it must be either released, when no longer needed, using the close function, or stored in a top-level variable and reused throughout the application

 *
 * */


/**
 * The Dispatchers.Unconned coroutine dispatcher starts a coroutine in the caller thread,
 * but only until the rst suspension point. After suspension it resumes the coroutine in the thread that is fully determined
 * by the suspending function that was invoked. The unconned dispatcher is appropriate for coroutines
 * which neither consume CPU time nor update any shared data (like UI) conned to a specic thread
 *
 * */

/**
 *  the dispatcher is inherited from the outer CoroutineScope by default.
 *  The default dispatcher for the runBlocking coroutine, in particular,
 *  is confined to the invoker thread, so inheriting it has the effect of conning execution to this thread with predictable FIFO scheduling.

 * */

fun demo2() = runBlocking<Unit> {

    launch(Dispatchers.Unconfined) { // not confined -- will work with main thread
        println("Unconfined      : I'm working in thread ${Thread.currentThread().name}")
        delay(500)
        println("Unconfined      : After delay in thread ${Thread.currentThread().name}")
    }

    launch { // context of the parent, main runBlocking coroutine
        println("main runBlocking: I'm working in thread ${Thread.currentThread().name}")
        delay(1000)
        println("main runBlocking: After delay in thread ${Thread.currentThread().name}")
    }

}




/**
 * Coroutines can suspend on one thread and resume on another thread. Even with a singlethreaded dispatcher
 * it might be hard to gure out what the coroutine was doing, where, and when. The common approach to debugging applications
 * with threads is to print the thread name in the log le on each log statement. This feature is universally supported by logging frameworks.
 *
 * When using coroutines, the thread name alone does not give much of a context, so kotlinx.coroutines includes debugging facilities
 * to make it easier.
 *
Run the following code with -Dkotlinx.coroutines.debug JVM option
 * */

/**
 * Debugging mode is also turned on when JVM is run with -ea option. You can read more about debugging facilities
 * in the documentation of the DEBUG_PROPERTY_NAME property.
 * */


fun demo3() = runBlocking<Unit> {

    val a = async {
        log("I'm computing a piece of the answer")
        6
    }

    val b = async {
        log("I'm computing another piece of the answer")
        7
    }

    log("The answer is ${a.await() * b.await()}")
}

fun log(msg: String) = println("[${Thread.currentThread().name}] $msg")


/**
 * It demonstrates several new techniques. One is using runBlocking with an explicitly specied context,
 * and the other one is using the withContext function to change the context of a coroutine while still staying in the same coroutine,
 * */

fun demo4() {

    newSingleThreadContext("Ctx1").use { ctx1 ->

        newSingleThreadContext("Ctx2").use { ctx2 ->

            runBlocking(ctx1) {

                log("Started in ctx1")

                withContext(ctx2) {
                    log("Working in ctx2")
                }

                log("Back to ctx1")
            }

        }

    }

}


/**
 * The coroutine's Job is part of its context, and can be retrieved from it using the coroutineContext[Job] expression:
 *
 * In the debug mode, it outputs something like this:
My job is "coroutine#1":BlockingCoroutine{Active}@6d311334
Note that isActive in CoroutineScope is just a convenient shortcut for coroutineContext[Job]?.isActive == true
 * */

fun demo5() = runBlocking<Unit> {

    println("My job is ${coroutineContext[Job]}  job = ${Job}")

}



fun demo6() = runBlocking<Unit> {

    // launch a coroutine to process some kind of incoming request
    val request = launch {
        // it spawns two other jobs, one with GlobalScope

        GlobalScope.launch {
            println("job1: I run in GlobalScope and execute independently!")
            delay(1000)
            println("job1: I am not affected by cancellation of the request")
        }

        // and the other inherits the parent context
        launch {
            delay(100)
            println("job2: I am a child of the request coroutine")
            delay(1000)
            println("job2: I will not execute this line if my parent request is cancelled")
        }
    }

    delay(500)
    request.cancel() // cancel processing of the request
    delay(1000) // delay a second to see what happens
    println("main: Who has survived request cancellation?")
}






/**
 * A parent coroutine always waits for completion of all its children.
 *
 * A parent does not have to explicitly track all the children it launches,
 * and it does not have to use Job.join to wait for them at the end
 * */

fun demo7() = runBlocking<Unit> {

    // launch a coroutine to process some kind of incoming request
    val request = launch {

        repeat(3) { i -> // launch a few children jobs
            launch  {
                delay((i + 1) * 200L) // variable delay 200ms, 400ms, 600ms
                println("Coroutine $i is done")
            }
        }

        println("request: I'm done and I don't explicitly join my children that are still active")
    }

    request.join() // wait for completion of the request, including all its children
    println("Now processing of the request is complete")
}



/**
 * Naming coroutines for debugging
 *
 * Automatically assigned ids are good when coroutines log often and you just need to correlate log records coming from the same coroutine.
 * However, when a coroutine is tied to the processing of a specic request or doing some specic background task,
 * it is better to name it explicitly for debugging purposes. The CoroutineName context element serves the same purpose as the thread name.
 *
 * It is included in the thread name that is executing this coroutine when the debugging mode is turned on
 *
 *  run with -Dkotlinx.coroutines.debug
 * */

fun demo8() = runBlocking(CoroutineName("main")) {

    log("Started main coroutine")

    // run two background value computations
    val v1 = async(CoroutineName("v1coroutine")) {
        delay(500)
        log("Computing v1")
        252
    }

    val v2 = async(CoroutineName("v2coroutine")) {
        delay(1000)
        log("Computing v2")
        6
    }

    log("The answer for v1 / v2 = ${v1.await() / v2.await()}")
}


/**
 * Combining context elements
 *
 * Sometimes we need to dene multiple elements for a coroutine context. We can use the + operator for that.
 *
 * For example, we can launch a coroutine with an explicitly specied dispatcher and an explicitly specied name at the same tim
 *
 * The output of this code with the -Dkotlinx.coroutines.debug JVM option is:
I'm working in thread DefaultDispatcher-worker-1 @test#2

 * */

fun demo9() = runBlocking<Unit> {

    launch(Dispatchers.Default + CoroutineName("test")) {
        println("I'm working in thread ${Thread.currentThread().name}")
    }
}

