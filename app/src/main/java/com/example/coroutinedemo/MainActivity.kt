package com.example.coroutinedemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        startScopeDemo()
        startCoroutineCancelDemo()

    }

    private fun startCoroutineCancelDemo() {
        val coroutineCancelDemo = CoroutineCancelDemo()
        coroutineCancelDemo.startDemo()
    }

    private fun startScopeDemo() {
        val scopeDemo = ScopeDemo()

    }
}
