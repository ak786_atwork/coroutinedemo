package com.example.coroutinedemo

import android.util.Log
import kotlinx.coroutines.*
import java.io.File
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

class CoroutineCancelDemo {

    val TAG = "CoroutineCancelDemo"


    fun startDemo() {

        val scope = CoroutineScope(Dispatchers.Default + Job())

        val job1 = scope.launch {
            showLog("job1 started")
            delay(2000)
            showLog("job1 ended")
        }
        val job2 = scope.launch {
            showLog("job2 started")
            delay(2000)
            /*this co routine will get cancelled without affecting other child coroutine*/
            //throw CancellationException("")
            showLog("job2 ended")
        }

        Thread.sleep(1000)

        /*cancelling job individually*/
//        job1.cancel("target achieved")

        scope.cancel()

        /*Once you cancel a scope, you won’t be able to launch new coroutines in the cancelled scope.*/
//        scope.launch {  showLog("job3 launched") }

    }


    private fun showLog(info: String) {
        Log.d(TAG, info)
    }

}