package com.example.coroutinedemo.async_operation

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*


fun foo(): List<Int> = listOf(1, 2, 3)

fun main() {

//    demo1()
//    demo2()
//    demo3()
//    demo4()
//    demo5()
//    demo6()
//    demo7()
//    demo8()
//    demo9()
//    demo10()
//    demo11()
//    demo12()
//    demo13()
//    demo14()
    demo15()


}

/**Representing multiple values
 *
 * Multiple values can be represented in Kotlin using collections
 * */

fun demo1() {
    foo().forEach { value -> println(value) }

}

/**Sequences
 *
 * If we are computing the numbers with some CPU-consuming blocking code (each computation taking 100ms),
 * then we can represent the numbers using a Sequence:

 * */

fun foo1(): Sequence<Int> = sequence {
    // sequence builder

    for (i in 1..3) {
        Thread.sleep(100) // pretend we are computing it
        yield(i) // yield next value
    }

}


fun demo2() {
    foo1().forEach { value -> println(value) }
}


/**Suspending functions
 *
 * However, this computation blocks the main thread that is running the code. When these values are computed by asynchronous code
 * we can mark the function foo with a suspend modier, so that it can perform its work without blocking and return the result as a list
 */

suspend fun foo_s(): List<Int> {

    delay(1000) // pretend we are doing something asynchronous here

    return listOf(1, 2, 3)

}


fun demo3() = runBlocking<Unit> {

    foo_s().forEach { value -> println(value) }

}


//--------------------------------------------------------------------------
/**Flows
 *
 * Using the List<Int> result type, means we can only return all the values at once.
 *
 * To represent the stream of values that are being asynchronously computed, we can use a Flow<Int> type
 * just like we would the Sequence<Int> type for synchronously computed values:

If you want to switch the context of execution of a flow, use the [flowOn] operator.
 * */

fun foo_f(): Flow<Int> = flow {
    // flow builder

    for (i in 1..3) {
        delay(100) // pretend we are doing something useful here
        emit(i) // emit next value
    }

}


fun demo4() = runBlocking<Unit> {

    // Launch a concurrent coroutine to check if the main thread is blocked
    launch {

        for (k in 1..3) {
            println("I'm not blocked $k")
            delay(100)
        }

    }

    // Collect the flow
    foo_f().collect { value -> println(value) }
}


/**
 * Notice the following differences in the code with the Flow from the earlier examples:
 *
 * A builder function for Flow type is called flow.
 * Code inside the flow { ... } builder block can suspend.
 * The function foo() is no longer marked with suspend modier.
 * Values are emitted from the ow using emit function.
 * Values are collected from the ow using collect function.
 *
 * We can replace delay with Thread.sleep in the body of foo's flow { ... } and see that the main thread is blocked in this case.
 *
 * */


//--------------------------------------------------------------------------------------------------

/**Flows are cold
 *
Flows are cold streams similar to sequences — the code inside a flow builder does not run until the flow is collected.
 * */


fun foo5(): Flow<Int> = flow {

    println("Flow started")

    for (i in 1..3) {
        delay(100)
        emit(i)
    }

}


fun demo5() = runBlocking<Unit> {

    println("Calling foo...")

    val flow = foo5()

    println("Calling collect...")

    flow.collect { value -> println(value) }

    println("Calling collect again...")

    flow.collect { value -> println(value) }

}

/**
 * This is a key reason the foo() function (which returns a ow) is not marked with suspend modifier.
 * By itself, foo() returns quickly and does not wait for anything.
 * The flow starts every time it is collected, that is why we see "Flow started" when we call collect again.
 * */


//-------------------------------------------------------------------------------------------------
/**Flow cancellation
 *
 * Flow adheres to the general cooperative cancellation of coroutines.
 *
 * However, flow infrastructure does not introduce additional cancellation points. It is fully transparent for cancellation.
 * As usual, flow collection can be cancelled when the flow is suspended in a cancellable suspending function (like delay),
 * and cannot be cancelled otherwise.
 *
 * The following example shows how the ow gets cancelled on a timeout when running in a withTimeoutOrNull block and stops executing its code
 *
 * */


fun foo6(): Flow<Int> = flow {

    for (i in 1..3) {
        delay(100)
        println("Emitting $i")
        emit(i)
    }

}


fun demo6() = runBlocking<Unit> {

    withTimeoutOrNull(250) {
        // Timeout after 250ms
        foo6().collect { value -> println(value) }
    }

    println("Done")
}

//--------------------------------------------------------------------------------------------------

/**Flow builders
 *
 * The flow { ... } builder from the previous examples is the most basic one.
 * There are other builders for easier declaration of flows: flowOf builder that defines a flow emitting a fixed set of values.
 *
 * Various collections and sequences can be converted to flows using .asFlow() extension functions.
 * */


fun demo7() = runBlocking<Unit> {

    // Convert an integer range to a flow

    (1..3).asFlow().collect { value -> println(value) }

}


//--------------------------------------------------------------------------------------------------

/**Intermediate flow operators
 *
 *Flows can be transformed with operators, just as you would with collections and sequences.
 * Intermediate operators are applied to an upstream ow and return a downstream flow.
 * These operators are cold, just like flows are. A call to such an operator is not a suspending function itself.
 * It works quickly, returning the definition of a new transformed ow.
— —
The basic operators have familiar names like map and filter.
The important difference to sequences is that blocks of code inside these operators can call suspending functions.

For example, a ow of incoming requests can be mapped to the results with the map operator,
even when performing a request is a long-running operation that is implemented by a suspending function
 *
 */

suspend fun performRequest(request: Int): String {

    delay(1000) // imitate long-running asynchronous work
    return "response $request"
}

fun demo8() = runBlocking<Unit> {

    (1..3).asFlow() // a flow of requests

        .map { request -> performRequest(request) }

        .collect { response -> println(response) }

}


//--------------------------------------------------------------------------------------------------

/**Transform operator
 *
 * Among the ow transformation operators, the most general one is called transform.
 * It can be used to imitate simple transformations like map and lter, as well as implement more complex transformations.
 * Using the transform operator, we can emit arbitrary values an arbitrary number of times.
 *
 * For example, using transform we can emit a string before performing a long-running asynchronous request and follow it with a response:
 * */



suspend fun performRequest9(request: Int): String {

    delay(1000) // imitate long-running asynchronous work
    return "response $request"

}

fun demo9() = runBlocking<Unit> {

    (1..3).asFlow() // a flow of requests

        .transform { request ->

            emit("Making request $request")
            emit(performRequest9(request))

        }

        .collect { response -> println(response) }

}

//--------------------------------------------------------------------------------------------------

/**Size-limiting operators
 *
 * Size-limiting intermediate operators like take cancel the execution of the ow when the corresponding limit is reached.
 * Cancellation in coroutines is always performed by throwing an exception,
 * so that all the resource-management functions (like try { ... } finally { ... } blocks) operate normally in case of cancellation
 * */


fun numbers(): Flow<Int> = flow {

    try {
        emit(1)
        emit(2)
        println("This line will not execute")
        emit(3)
    } finally {
        println("Finally in numbers")
    }

}



fun demo10() = runBlocking<Unit> {

    numbers()
        .take(2) // take only the first two
        .collect { value -> println(value) }
}

//--------------------------------------------------------------------------------------------------

/**Terminal flow operators
 *
 * Terminal operators on flows are suspending functions that start a collection of the flow.
 *
 * The collect operator is the most basic one, but there are other terminal operators,
 * which can make it easier: Conversion to various collections like toList and toSet.
 * Operators to get the rst value and to ensure that a ow emits a single value. Reducing a ow to a value with reduce and fold.
 * */


fun demo11() = runBlocking<Unit> {

    val sum = (1..5).asFlow()
        .map { it * it } // squares of numbers from 1 to 5
        .reduce { a, b -> a + b } // sum them (terminal operator)

    println(sum)
}

//--------------------------------------------------------------------------------------------------

/**Flows are sequential
 *
 * Each individual collection of a flow is performed sequentially unless special operators that operate on multiple flows are used.
 *
 * The collection works directly in the coroutine that calls a terminal operator. No new coroutines are launched by default.
 * Each emitted value is processed by all the intermediate operators from upstream to downstream
 * and is then delivered to the terminal operator after.
 *
 * See the following example that lters the even integers and maps them to strings:
 * */

fun demo12() = runBlocking<Unit> {

    (1..5).asFlow()
        .filter {
            println("Filter $it")
            it % 2 == 0
        }
        .map {
            println("Map $it")
            "string $it"
        }.collect {
            println("Collect $it")
        }

}


//--------------------------------------------------------------------------------------------------

/**Flow context
 *
 * Collection of a flow always happens in the context of the calling coroutine.
 * For example, if there is a foo flow, then the following code runs in the context specified by the author of this code,
 * regardless of the implementation details of the foo flow
 *
 * withContext(context) {
 *      foo.collect {
 *          value ->        println(value) // run in the specified context
 *      }
 *  }
 *
 *This property of a ow is called context preservation.
 *
 * So, by default, code in the flow { ... } builder runs in the context that is provided by a collector of the corresponding flow.
 * For example, consider the implementation of foo that prints the thread it is called on and emits three numbers:
 *
 * */


fun log(msg: String) = println("[${Thread.currentThread().name}] $msg")

fun foo13(): Flow<Int> = flow {

    log("Started foo flow")

    for (i in 1..3) {
        emit(i)
    }
}

fun demo13() = runBlocking<Unit> {

    foo13().collect { value -> log("Collected $value") }

}

/**
 * Since foo().collect is called from the main thread, the body of foo's ow is also called in the main thread.
 *
 * This is the perfect default for fast-running or asynchronous code that does not care about the execution context
 * and does not block the caller.
 * */

//--------------------------------------------------------------------------------------------------

/**Wrong emission withContext
 *
 * However, the long-running CPU-consuming code might need to be executed in the context of Dispatchers.Default
 * and UI-updating code might need to be executed in the context of Dispatchers.Main.
 *
 * Usually, withContext is used to change the context in the code using Kotlin coroutines,
 * but code in the flow { ... } builder has to honor the context preservation property and is not allowed to emit from a different context
 * */

fun foo14(): Flow<Int> = flow {

    // The WRONG way to change context for CPU-consuming code in flow builder
    kotlinx.coroutines.withContext(Dispatchers.Default) {
        for (i in 1..3) {
            Thread.sleep(100) // pretend we are computing it in CPU-consuming way
            emit(i) // emit next value
        }
    }

}

fun demo14() = runBlocking<Unit> {
//    foo14().collect { value -> println(value) }   //it will throw exception
}

/**
 * Note that we had to use a fully qualified name of the kotlinx.coroutines.withContext function in this example to demonstrate this exception.
 *
 * A short name of withContext would have resolved to a special stub function that produces a compilation error
 * to prevent us from running into this problem.
 * */


//--------------------------------------------------------------------------------------------------

/**flowOn operator
 *
 * The exception refers to the flowOn function that shall be used to change the context of the flow emission.
 * The correct way to change the context of a flow is shown in the example below,
 * which also prints the names of the corresponding threads to show how it all works:

 * */

fun foo15(): Flow<Int> = flow {

    for (i in 1..3) {
        Thread.sleep(100) // pretend we are computing it in CPU-consuming way
        log("Emitting $i")
        emit(i) // emit next value
    }

}.flowOn(Dispatchers.Default) // RIGHT way to change context for CPU-consuming code in flow builder


fun demo15() = runBlocking<Unit> {

    foo15().collect { value ->
        log("Collected $value")
    }

}

/**
 * Another thing to observe here is that the owOn operator has changed the default sequential nature of the flow.
 * Now collection happens in one coroutine ("coroutine#1") and emission happens in another coroutine ("coroutine#2")
 * that is running in another thread concurrently with the collecting coroutine.
 *
 * The flowOn operator creates another coroutine for an upstream ow when it has to change the CoroutineDispatcher in its context.
 * */

