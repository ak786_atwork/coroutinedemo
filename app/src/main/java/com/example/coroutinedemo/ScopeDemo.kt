package com.example.coroutinedemo

import android.util.Log
import kotlinx.coroutines.*

class ScopeDemo {

    val TAG = "SCOPE_DEMO"

    constructor() {
        startCoroutine()
    }

    public fun startCoroutine() {
        val scope = CoroutineScope(Job() + Dispatchers.Main)
        val job = scope.launch {
            // New coroutine that has CoroutineScope as a parent

            showLog("coroutine started by launch")

            val result = async {
                // New coroutine that has the coroutine started by
                // launch as a parent

                showLog((this == scope).toString())

                showLog("coroutine started by async")


            }.await()
        }
    }


    fun showLog(info: String) {
        Log.d(TAG, info)
    }

}