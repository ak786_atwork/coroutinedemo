package com.example.coroutinedemo.exception

import kotlinx.coroutines.*
import java.io.IOException
import java.lang.Exception

fun main() {
//    demo1()
//    demo2()
//    demo3()
//    demo4()
//    demo5()
//    demo6()
//    demo7()
    demo8()
//    demo9()
}


/**Exception propagation

Coroutine builders come in two favors: propagating exceptions automatically (launch and actor) or exposing them to users (async and produce).
The former treat exceptions as unhandled, similar to Java's Thread.uncaughtExceptionHandler,
while the latter are relying on the user to consume the nal exception, for example via await or receive


 */

fun demo1() = runBlocking {

    val job = GlobalScope.launch {
        println("Throwing exception from launch ${Thread.currentThread().name}")
        throw IndexOutOfBoundsException() // Will be printed to the console by Thread.defaultUncaughtExceptionHandler
    }

    job.join()
    println("Joined failed job")

    val deferred = GlobalScope.async {
        println("Throwing exception from async ${Thread.currentThread().name}")
        throw ArithmeticException() // Nothing is printed, relying on user to call await
    }

    try {
        deferred.await()
        println("Unreached")
    } catch (e: ArithmeticException) {
        println("Caught ArithmeticException")
    }

}


//--------------------------------------------------------------------------------------------------
/**CoroutineExceptionHandler
 *
 * But what if one does not want to print all exceptions to the console?
 * CoroutineExceptionHandler context element is used as generic catch block of coroutine where custom logging or exception handling may take place.
 *
 * It is similar to using Thread.uncaughtExceptionHandler.

On JVM it is possible to redene global exception handler for all coroutines by registering CoroutineExceptionHandler via ServiceLoader.
Global exception handler is similar to Thread.defaultUncaughtExceptionHandler which is used when no more specic handlers are registered.

On Android, uncaughtExceptionPreHandler is installed as a global coroutine exception handler. CoroutineExceptionHandler is invoked only
on exceptions which are not expected to be handled by the user, so registering it in async builder and the like of it has no effect.
 *
 *
 * */

fun demo2() = runBlocking {

    val handler = CoroutineExceptionHandler { _, exception ->
        println("Caught $exception")
    }

    val job = GlobalScope.launch(handler) {
        throw AssertionError()
    }

    val deferred = GlobalScope.async(handler) {
        throw ArithmeticException() // Nothing will be printed, relying on user to call deferred.await()
    }

    joinAll(job, deferred)
}


//--------------------------------------------------------------------------------------------------

/**Cancellation and exceptions
Cancellation is tightly bound with exceptions.

Coroutines internally use CancellationException for cancellation, these exceptions are ignored by all handlers, so they should be
used only as the source of additional debug information, which can be obtained by catch block.

When a coroutine is cancelled using Job.cancel, it terminates, but it does not cancel its parent

 */


fun demo3() = runBlocking {

    val job = launch {
        val child = launch {
            try {
                delay(Long.MAX_VALUE)
            } catch (exception: Exception) {
                println("inside catch")
            } finally {
                println("Child is cancelled")
            }
        }

        yield()
        println("Cancelling child")
        child.cancel()
        child.join()
        yield()

        println("Parent is not cancelled")
    }
    job.join()

}

//--------------------------------------------------------------------------------------------------


/**
If a coroutine encounters an exception other than CancellationException, it cancels its parent with that exception.
This behaviour cannot be overridden and is used to provide stable coroutines hierarchies for structured concurrency
which do not depend on CoroutineExceptionHandler implementation.

The original exception is handled by the parent when all its children terminate.


This also a reason why, in these examples, CoroutineExceptionHandler is always installed to a coroutine that is created in GlobalScope.
It does not make sense to install an exception handler to a coroutine that is launched in the scope of the main runBlocking,
since the main coroutine is going to be always cancelled when its child completes with exception despite the installed handler.

 * */


fun demo4() = runBlocking {

    val handler = CoroutineExceptionHandler { _, exception ->
        println("Caught $exception")
    }

    val job = GlobalScope.launch(handler) {

        launch {
            // the first child
            try {
                delay(Long.MAX_VALUE)
            } finally {
                withContext(NonCancellable) {
                    println("Children are cancelled, but exception is not handled until all children terminate")
                    delay(100)
                    println("The first child finished its non cancellable block")
                }
            }
        }

        launch {
            // the second child
            delay(10)
            println("Second child throws an exception")
            throw ArithmeticException()
        }
    }
    job.join()
}


//--------------------------------------------------------------------------------------------------

/**Exceptions aggregation
 *
 * What happens if multiple children of a coroutine throw an exception?
 * The general rule is "the rst exception wins", so the rst thrown exception is exposed to the handler.
 * But that may cause lost exceptions, for example if coroutine throws an exception in its finally block.
 * So, additional exceptions are suppressed.


One of the solutions would have been to report each exception separately, but then Deferred.await should have had
the same mechanism to avoid behavioural inconsistency and this would cause implementation details of a coroutines
(whether it had delegated parts of its work to its children or not) to leak to its exception handler.

Note: This above code will work properly only on JDK7+ that supports suppressed exceptions
 * */


fun demo5() = runBlocking {

    val handler = CoroutineExceptionHandler { _, exception ->
        println("Caught $exception with suppressed ${exception.suppressed.contentToString()}")
    }

    val job = GlobalScope.launch(handler) {
        launch {
            try {
                delay(Long.MAX_VALUE)
            } finally {
                throw ArithmeticException()
            }
        }

        launch {
            delay(100)
            throw IOException()
        }
        delay(Long.MAX_VALUE)
    }

    job.join()
}


//--------------------------------------------------------------------------------------------------
/**
 *Cancellation exceptions are transparent and unwrapped by default
 * */

fun demo6() = runBlocking {

    val handler = CoroutineExceptionHandler { _, exception ->
        println("Caught original $exception")
    }

    val job = GlobalScope.launch(handler) {
        val inner = launch {
            launch {
                launch {
                    throw IOException()
                }
            }
        }

        try {
            inner.join()
        } catch (e: CancellationException) {
            println("Rethrowing CancellationException with original cause")
            throw e
        }
    }

    job.join()
}


//--------------------------------------------------------------------------------------------------

/**    Supervision
 *
 * As we have studied before, cancellation is a bidirectional relationship propagating through the whole coroutines hierarchy.
 * But what if unidirectional cancellation is required?
 *
 * A good example of such a requirement is a UI component with the job dened in its scope.
 * If any of the UI's child tasks have failed, it is not always necessary to cancel (eectively kill) the whole UI component,
 * but if UI component is destroyed (and its job is cancelled), then it is necessary to fail all child jobs as their results are no longer required.
 *
 * Another example is a server process that spawns several children jobs and needs to supervise their execution,
 * tracking their failures and restarting just those children jobs that had failed.

For these purposes SupervisorJob can be used. It is similar to a regular Job with the only exception that cancellation
is propagated only downwards
 * */

fun demo7() = runBlocking {

    val supervisor = SupervisorJob()

    with(CoroutineScope(coroutineContext + supervisor)) {
        // launch the first child -- its exception is ignored for this example (don't do this in practice!)

        val firstChild = launch(CoroutineExceptionHandler { _, _ ->  }) {
            println("First child is failing")
            throw AssertionError("First child is cancelled")
        }

        // launch the second child
        val secondChild = launch {

            firstChild.join()
            // Cancellation of the first child is not propagated to the second child
            println("First child is cancelled: ${firstChild.isCancelled}, but second one is still active")
            try {
                delay(Long.MAX_VALUE)
            } finally {
                // But cancellation of the supervisor is propagated
                println("Second child is cancelled because supervisor is cancelled")
            }
        }
        // wait until the first child fails & completes
        firstChild.join()
        println("Cancelling supervisor")
        supervisor.cancel()
        secondChild.join()
    }

}

//--------------------------------------------------------------------------------------------------
/**.
Supervision scope

For scoped concurrency supervisorScope can be used instead of coroutineScope for the same purpose.
It propagates cancellation only in one direction and cancels all children only if it has failed itself.
It also waits for all children before completion just like coroutineScope does.

 */

fun demo8() = runBlocking {
    try {
        supervisorScope {
            val child = launch {
                try {
                    println("Child is sleeping")
                    delay(Long.MAX_VALUE)
                } finally {
                    println("Child is cancelled")
                }
            }

            // Give our child a chance to execute and print using yield
            yield()
            println("Throwing exception from scope")
            throw AssertionError()
        }
    } catch(e: AssertionError) {
        println("Caught assertion error")
    }
}


//--------------------------------------------------------------------------------------------------
/**Exceptions in supervised coroutines
 *
 * Another crucial difference between regular and supervisor jobs is exception handling.
 * Every child should handle its exceptions by itself via exception handling mechanisms.
 * This difference comes from the fact that child's failure is not propagated to the parent.
 *
 * */


fun demo9() = runBlocking {

    val handler = CoroutineExceptionHandler { _, exception ->
        println("Caught $exception")
    }

    supervisorScope {
        val child = launch(handler) {
            println("Child throws an exception")
            throw AssertionError()
        }
        println("Scope is completing")
    }
    println("Scope is completed")
}




